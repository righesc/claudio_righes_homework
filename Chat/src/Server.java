
// Fig. 18.4: Server.java
// Set up a Server that will receive a connection from a client, send 
// a string to the client, and close the connection.
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Server extends JFrame {
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField enterField;
   private JTextArea displayArea;
   private ObjectInputStream input;
   private ServerSocket server;
   private Socket connection;
   private ObjectOutputStream output;
   private int counter = 1;
   ThreadServer t[]=new ThreadServer[5];
   String s;

   // set up GUI
   public Server()
   {
      super( "Server" );
      try {
		server = new ServerSocket( 12345, 100 );
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      int i=0;
      while(true){
          try {
			connection = server.accept();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // allow server to accept connection             	  
          try {
			output = new ObjectOutputStream( connection.getOutputStream() );
			output.flush(); 
			input = new ObjectInputStream( connection.getInputStream() );
			
			//partenza thread
			(t[i]=new ThreadServer(t,s)).start();;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	 i++; 
      }

   } // end Server constructor

   // set up and run server 

   public static void main( String args[] )
   {
      Server application = new Server();

   }

}  // end class Server

/**************************************************************************
 * (C) Copyright 1992-2003 by Deitel & Associates, Inc. and               *
 * Prentice Hall. All Rights Reserved.                                    *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/