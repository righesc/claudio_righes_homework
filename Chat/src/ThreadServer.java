import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ThreadServer extends Thread{

	ThreadServer []t= new ThreadServer[5];
	String con;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	public ThreadServer(ThreadServer[] t, String s) {
		// TODO Auto-generated constructor stub
		this.t=t;
		con=s;
	}

	public void run() {
		super.run();
		System.out.println("thread " + getName() + " runs");
		boolean done = false;
		while(!done) {
			long time = 1; // scrittura ogni time millisec
			try {
				sleep((long) (Math.random()*time));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			try {
				synchronized (output) {
					output.writeObject(con);
					output.flush();
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				done = true; //esce dal ciclo quando il client si disconnette
			}
		}
	}

}
