import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class ServerN {

	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket server;
	private Socket connection;

	private int port = 12345;
	int n=2;
			//(int)(Math.random()*10);
	private String numero =Integer.toString(n);
	
	public ServerN() {
		
		
		try {
			// Step 1: Create a ServerSocket.
			server = new ServerSocket(port);

			//step 2: attesa connessione
			String message = "server in attesa sulla porta " + port  ;
			System.out.println(message);
			connection = server.accept(); //il programma si blocca
			
			message =  "Connesso con " +
					connection.getInetAddress().getHostName();
			System.out.println(message);

			//step 3: ottenere gli stream
			output = new ObjectOutputStream(connection.getOutputStream() );
			input = new ObjectInputStream(connection.getInputStream());

			System.out.println("Stream ottenuti");

			//step 4: comunicazione
			
			//lettura password
			System.out.println("Hai due tentativi, n da 0 a 10");
			int i=0;
			String string;
			do{
				System.out.println("attesa numero");
				string = (String) input.readObject();
				System.out.println("Letta numero: " + string);
				
				System.out.println("invio risposta");
					if(string.equals(numero)) {
						output.writeObject("numero corretto");
						output.flush();
						break;

					} else {
						output.writeObject("numero errato");
						output.flush();
					}
						i++;
						}while((i<2)||(string.equals(numero)));
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			// Step 5: chiusura della connessione
			System.out.println("chiusura connessione");
			try {
				connection.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if(output != null) output.close();
			}
			catch( IOException ioException ) {
				ioException.printStackTrace();
			}   


			try {
				if(input != null)input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


	}
	
	
	public static void main( String args[] )
	{
		new ServerN();

	}
}
