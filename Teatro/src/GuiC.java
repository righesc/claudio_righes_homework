import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.Position;

import java.awt.Font;

public class GuiC {
	JFrame start=new JFrame("Login");
	private JTextField textField;
	static JFrame f=new JFrame("Sistema prenotazione cinema");
	static JFrame posti=new JFrame("Posti liberi");
	
	ClientTeatr ct;
	
	public GuiC(ClientTeatr ct) {
		this.ct=ct;
		
		start.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(97, 92, 245, 49);
		start.getContentPane().add(textField);
		textField.setColumns(10);
		
		JTextPane txtpnInserisciLaTua = new JTextPane();
		txtpnInserisciLaTua.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtpnInserisciLaTua.setText("Inserisci la tua email");
		txtpnInserisciLaTua.setBounds(143, 29, 147, 28);
		start.getContentPane().add(txtpnInserisciLaTua);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.getSource()==btnLogin)
					ct.login(textField.getText());
					start.setVisible(false);
					f.setVisible(true);
			}
			
		});
		btnLogin.setBounds(172, 179, 97, 25);
		start.getContentPane().add(btnLogin);
		start.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		start.setVisible(true);
		start.setSize(438, 249);
		//-----------------------------------------------
		f.getContentPane().setLayout(null);
		
		JButton btnPrenota = new JButton("Prenota");
		btnPrenota.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.getSource()==btnPrenota)
					ct.prenota();
			}
			
		});
		btnPrenota.setBounds(166, 34, 111, 25);
		f.getContentPane().add(btnPrenota);
		
		JButton btnDisdicicambiaPosto = new JButton("Disdici/cambia Posto");
		btnDisdicicambiaPosto.setBounds(140, 85, 166, 25);
		f.getContentPane().add(btnDisdicicambiaPosto);
		

		JButton btnGuardaPostiPrenotati = new JButton("Guarda posti prenotati");
		btnGuardaPostiPrenotati.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
					
			}
			
		});
		btnGuardaPostiPrenotati.setBounds(140, 142, 166, 25);
		f.getContentPane().add(btnGuardaPostiPrenotati);
		
		JButton btnEsci = new JButton("Esci");
		btnEsci.setBounds(311, 195, 97, 25);
		f.getContentPane().add(btnEsci);
		btnEsci.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.getSource()==btnEsci)
					ct.chiudi();
			}
			
		});
		// TODO Auto-generated constructor stub
		f.setVisible(false);
		f.setSize(450,280);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void displayPostiLiberi(Object object) {
		// TODO Auto-generated method stub
		f.setVisible(false);
		posti.setVisible(true);
		
	}
}
