package prog3;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;

import javax.swing.*;

public class Prog3 {

		JPanel cards; //a panel that uses CardLayout

	    public void addComponentToPane(Container pane) {
	        //Put the JComboBox in a JPanel to get a nicer look.
	        JPanel comboBoxPane = new JPanel(); //use FlowLayout
	        String comboBoxItems[] = { "Classi", "Alunno", "Assegnazione A-C"};
	        JComboBox cb = new JComboBox(comboBoxItems);
	        
	        cb.setEditable(false);
	        
	        cb.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					 CardLayout cl = (CardLayout)(cards.getLayout());
				     cl.show(cards, (String)cb.getSelectedItem());
					
				}

			});
	        comboBoxPane.add(cb);
	         
	        //Create the "cards".
	        JPanel card1 = new JPanel();
	        card1.add(new JButton("Button 1"));
	        card1.add(new JButton("Button 2"));
	        card1.add(new JButton("Button 3"));
	         
	        JPanel card2 = new JPanel();
	        card2.add(new JTextField("TextField", 20));
	         
	        
	        JPanel card3=new JPanel();
	        
	        //Create the panel that contains the "cards".
	        cards = new JPanel(new CardLayout());
	        cards.add(card1, "Classi");
	        cards.add(card2, "Alunno");
	        cards.add(card3, "Assegnazione A-C");
	        card3.setLayout(null);
	        
	        JComboBox comboBox = new JComboBox();
	        comboBox.setBounds(138, 13, 241, 22);
	        card3.add(comboBox);
	        
	        JComboBox comboBox_1 = new JComboBox();
	        comboBox_1.setBounds(138, 63, 124, 22);
	        card3.add(comboBox_1);
	        
	        JLabel lblAnno = new JLabel("Anno");
	        lblAnno.setBounds(52, 66, 56, 16);
	        card3.add(lblAnno);
	        
	        JLabel lblNominativo = new JLabel("Nominativo");
	        lblNominativo.setBounds(52, 16, 74, 16);
	        card3.add(lblNominativo);
	        
	        JComboBox comboBox_2 = new JComboBox();
	        comboBox_2.setBounds(138, 98, 124, 22);
	        card3.add(comboBox_2);
	        
	        JLabel lblNewLabel = new JLabel("Sezione");
	        lblNewLabel.setBounds(52, 101, 74, 16);
	        card3.add(lblNewLabel);
	         
	        pane.add(comboBoxPane, BorderLayout.PAGE_START);
	        pane.add(cards, BorderLayout.CENTER);
	    }
	     
	    public void itemStateChanged(ItemEvent evt) {
	       
	    }
	     
	    /**
	     * Create the GUI and show it.  For thread safety,
	     * this method should be invoked from the
	     * event dispatch thread.
	     */
	    private static void createAndShowGUI() {
	        //Create and set up the window.
	        JFrame frame = new JFrame("CardLayoutDemo");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	         
	        //Create and set up the content pane.
	        Prog3 demo = new Prog3();
	        demo.addComponentToPane(frame.getContentPane());
	         
	        //Display the window.
//	        frame.pack();
	        frame.setVisible(true);
	    }
	     
	    public static void main(String[] args) {
	        
	        //Schedule a job for the event dispatch thread:
	        //creating and showing this application's GUI.
	        javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	            }
	        });
	    }
		}


