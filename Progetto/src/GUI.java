import javax.swing.JFrame;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;

public class GUI {
	private JTextField textField;
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/scuola";
	static final String USER = "root";
	static final String PASS = "";
    Connection conn = null;
    Statement stmt = null;
    JComboBox comboBox;
    JComboBox comboBox_1;
    String alfa[]={"a","b","c","d","e","f","g","h","i","l","m","n","o","p","q","r","s","t","u","v","w"};
	String ind[]={"informatica","meccanica","occhiale","elettronica","meccatronica","edile","elettrotecnica"};
    String sezione = null;
    String indirizzo = null;
    String anno;
    public GUI() {
		// TODO Auto-generated constructor stub
		
	       try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


	    System.out.println("Connecting to database...");
	    try {
			conn = DriverManager.getConnection(DB_URL,USER,PASS);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	    System.out.println("Creating statement...");
	    try {
			stmt = conn.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		
		JFrame f=new JFrame("Classi");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(32, 44, 116, 22);
		f.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblAnno = new JLabel("Anno");
		lblAnno.setBounds(32, 13, 56, 16);
		f.getContentPane().add(lblAnno);
		
		JLabel lblSezione = new JLabel("Sezione");
		lblSezione.setBounds(266, 15, 56, 16);
		f.getContentPane().add(lblSezione);
		
		JLabel lblIndirizzo = new JLabel("Indirizzo");
		lblIndirizzo.setBounds(181, 100, 56, 16);
		f.getContentPane().add(lblIndirizzo);
		
		comboBox = new JComboBox(alfa);
		comboBox.setBounds(266, 44, 133, 22);
		f.getContentPane().add(comboBox);
		
		comboBox_1 = new JComboBox(ind);
		comboBox_1.setBounds(153, 141, 133, 22);
		f.getContentPane().add(comboBox_1);
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(302, 193, 97, 25);
		btnSave.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				anno=textField.getText();
				sezione=comboBox.getSelectedItem().toString();
				indirizzo=(String) comboBox_1.getSelectedItem();
			 try{
			    String sql = "INSERT INTO classe (id, anno, sezione, indirizzo) VALUES ('"+anno+"','"+sezione+"', '"+indirizzo+"');";
			    stmt.executeQuery(sql);
			    stmt.close();
			    conn.close();
			 }catch(SQLException se){
			    se.printStackTrace();
			 }catch(Exception e){
			    e.printStackTrace();
			 }finally{
			    try{
			           if(stmt!=null)
			              stmt.close();
			    }catch(SQLException se2){

			    }
			    try{
			        if(conn!=null)
			                 conn.close();
			    }catch(SQLException se){
			              se.printStackTrace();
			    }
			 }
			}
			
		});
		f.getContentPane().add(btnSave);
		
		JButton btnReset = new JButton("Reset");
		btnReset.setBounds(51, 193, 97, 25);
		f.getContentPane().add(btnReset);
		btnReset.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				textField.setText("");
				comboBox.setSelectedIndex(0);
				comboBox_1.setSelectedIndex(0);
			}
			
		});
		
		
		f.setVisible(true);
	}
}
